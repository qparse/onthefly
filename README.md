# On-the-fly music transcription client

## Features:

* Receive MIDI messages from a MIDI Input
* Or to create MIDI messages from a Keyboard displayed in the screen
* Send buffers of MIDI messages through a POST request
* Receive as a response a MEI file with the music scores
* Display music scores in SVG format
* Play the sound of the pressed keys
* Select the instrument sound
* Select the BPM and time signature that the performance will have
* Metronome

## Components:

### Keyboard

Component responsible of displaying the screen keyboard

Props:

* addMidiEvent: function to add a new MIDI messafe to the buffer
* playNote: function to start the sound of a note.
* stopNote: function to stop the sound of a note.
* resumeAudio: function to restart the sound of all notes.
* stopAllNotes: function to stop the sound of all notes.
* initialOctave: number --> indicates in which octave the keyboard will start
* numberOfOctaves: number --> indicates how many octaves the keyboard will have
* handleIsPlaying: set a new value in the isPlaying parent state.

State:

* keysNumbers: dictionary --> {'white': numbers[], 'black': numbers[]}
* initialTime: number --> timestamp of the first key pressed.

Methods:

| __Name__         | __parameters__                                   | __description__                                                                              |
| ---------------- | ------------------------------------------------ | -------------------------------------------------------------------------------------------- |
| createKeyNumbers | initialOctave (number), numberOfOctaves (number) | Creates a dictionary with the position of each key of the keyboard                           |
| handleMouseDown  | event                                            | Creates a MIDI message associated with the key pressed and emits the associated sound        |
| handleMouseUp    | event                                            | Creates a MIDI message associated with the release of the key and stops the associated sound |
| handlePlay       | -                                                | Changes the state of the parent to let it know that the key is pressed                       |
| handleStop       | -                                                | Changes the state of the parent to let it know that the key is released                      |

### Metronome

Component responsible of sending the new time signature and BPM, and starting or stoping the sound of the metronome. It displays the time signature and bpm option in the navbar and the associated button to save the values.

Props:

* bpm: number --> indicates the beats per minute selected
* isPlaying: boolean --> indicates if the user started playing
* setBPM: function to set the new selected value of bpm
* setMetronomeState: function;
* signature: number --> indicates the time signature selected
* setSignature: function to set the new selected value of the time signature
* handleSubmit: function to send the new values of bpm and time signature to the parent

State:

* isPlaying: boolean --> indicates if the user started playing (same as props)
* count: number --> keeps the track of clicks of the metronome
* bpm: number --> indicates the beats per minute selected (same as props)
* beatsPerMeasure: number --> indicates the number of beats that each measure has

Methods:

| __Name__            | __parameters__ | __description__                                                                              |
| ------------------- | -------------- | -------------------------------------------------------------------------------------------- |
| handleInputChange   | event          | changes the tempo value                                                                      |
| handleDecreaseTempo | -              | decreases the tempo one bpm                                                                  |
| handleIncreaseTempo | -              | increases the tempo one bpm                                                                  |
| handleClick         | -              | sends the new bpm and time signature values to the parent, and starts or stops the metronome |
| handleSelectChange  | event          | changes the value of the time signature                                                      |
| playClick           | -              | plays the sound of each click of the metronome                                               |
| startStop           | -              | starts or stops the metronome                                                                |

### MIDIComponent

Component responsible of connecting to the MIDI keyboard and receiving the MIDI messages.

Props:

* addMidiEvent: funtion to add a new MIDI message to the buffer
* playNote: function to start the sound of a note.
* stopNote: function to stop the sound of a note.
* resumeAudio: function to restart the sound of all notes.
* stopAllNotes: function to stop the sound of all notes.
* handleIsPlaying: function to set a new value in the isPlaying parent state.
* firstEventTime: number --> indicates the time of the first key pressed.
* setFirstEventTime: function to set into the parent the time of the first key pressed.

State:

* midiConnected: boolean --> indicates if a MIDI keyboard is connected or not.

### MusicScore

Component responsible of displaying the music score.

Props:

* file: XMLMusic file that will be converted into a music score.

### TimerLogic

Component responsible of sending the buffers of MIDI messages to the server every X amount of time.

Props: 

* seconds: number --> indicates the seconds that have to pass to send a buffer to the server.
* midiEvents: object --> {'event': { 'flag': string, 'key': number, 'velocity': number, 'timestamp': number} }
* firstEvent: number --> indicates the timestamp of the first key pressed;
* setFirstEvent: function to set the timestamp of the first key pressed on the parent;
* isPlaying: boolean --> indicates if the user is playing or not;

| __Name__ | __parameters__ | __description__                                            |
| -------- | -------------- | ---------------------------------------------------------- |
| sendData | -              | callback to send a POST request to the API with the buffer |
| timer    | -              | callback that runs every X amount of seconds               |

### Components of the menu form:

* Metronome also has a Form.

#### KeyboardForm

Component responsible of the options associated with the keyboard, i.e., the sound of the instrument, the number of octaves and the initial octave.

Props:

* instrumentName: any --> string that indicates the name of the selected instrument sound
* initial: number --> indicates the initial octave selected for the keybaord
* number: number --> indicates the number of octaves selected for the keybaord
* setInstrumentName: function to set the instrument selected in the parent
* changeOctave: function to set the octaves options in the parent

State:

* name: string --> indicates the name of the selected instrument sound (same as props)
* initialO: number --> indicates the initial octave selected for the keybaord (same as props)
* numberO:  number --> indicates the number of octaves selected for the keybaord (same as props)

Methods:
|__Name__         |__parameters__|__description__|
|-----------------|--------------|---------------|
|handleSelectChange| event | handles the changes of the instrument sound |
|handleOctaveChange| event | handles the changes of the number of octaves and the initial octave |

#### ModeForm

Component responsible of the selection of the type of keyboard, MIDI or screen.

Props:

* mode: string --> indicates the options (MIDI or screen)
* setMode: funtion to set the selected option in the parent.

Method:
|__Name__         |__parameters__|__description__|
|-----------------|--------------|---------------|
| handleSelectChange | event | handles the changes in the type of keyboard |

## MIDI Messages buffer format:

The MIDI message is in JSON format, it has a root key named __events__, and its value is an array of objects. Each object has the following structure:

```javascript
{
    "key": int (0, 127),
    "flag" string ("ON", "OFF"),
    "velocity" int (0, 127), 
    "timestamp": int (0, inf) (in miliseconds)
}
```

An example of a buffer is shown below:

```json
 { 
     "events": [
         {
             "key": 70,
             "flag": "ON",
             "velocity": 50,
             "timestamp": 0
         },
         {
             "key": 70,
             "flag": "OFF",
             "velocity": 0,
             "timestamp": 130
         },
     ]
 } 
```