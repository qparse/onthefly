import React from 'react';
import './App.css';


import { InstrumentName } from 'soundfont-player';
import SoundFontProvider from './components/Audio/SoundFontProvider';
import Metronome from './components/Metronome';
import KeyboardForm from './components/KeyboardForm';
import Timer from './components/TimerLogic';
import ModeForm from './components/ModeForm';
// import VerovioViz from './components/verovio'
import MusicScore from './components/MusicScore';


interface IState {
  params: {
    signature: number,
    bpm: number,
    seconds: number,
  };
  midiEvents: IMidiEvent['event'][];
  firstEvent: number;
  firstEventTime: number;
  isPlaying: boolean;
  instrumentName: InstrumentName;
  numberOfOctaves: number;
  initialOctave: number;
  mode: string;
  playMetronome: boolean;
}

export interface IMidiEvent {
  event: {
    flag: string,
    key: number,
    velocity: number,
    timestamp: number
  }
}

// TO DO:
// - Mejorar estética del navbar
// - Metronome

class App extends React.Component<any,IState> {
  constructor(props: any) {
    super(props);

    this.state = {
      params: {
        signature: 2,
        bpm: 60,
        seconds: 0,
      },
      midiEvents: [],
      firstEvent: 0,
      firstEventTime: 0,
      isPlaying: false,
      instrumentName: "acoustic_grand_piano",
      numberOfOctaves: 4,
      initialOctave: 2,
      mode: 'MIDI',
      playMetronome: false,
    };
    this.handleBpmUpdate = this.handleBpmUpdate.bind(this);
    this.handleSignatureUpdate = this.handleSignatureUpdate.bind(this);
    this.handleMidiEventsUpdate = this.handleMidiEventsUpdate.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleKeyboardOctaves = this.handleKeyboardOctaves.bind(this);
    this.setInstrumentName = this.setInstrumentName.bind(this);
    this.handleIsPlaying = this.handleIsPlaying.bind(this);
    this.setFirstEvent = this.setFirstEvent.bind(this);
    this.setFirstEventTime = this.setFirstEventTime.bind(this);
    this.setMode = this.setMode.bind(this);
    this.setMetronomeState = this.setMetronomeState.bind(this);
  };

  // Function to handle the submit of the tempo parameter
  handleSubmit = (signature: number, bpm: number) => {
    const seconds = Math.trunc(1000 * (60 * signature) / bpm);
    this.setState({ params: { 
      seconds: seconds,
      signature: signature,
      bpm: bpm,
    } });
  };

  // handle the updates of the bpm parameter
  handleBpmUpdate = (bpmm: number) => {
    const { bpm, signature } = this.state.params;
    const seconds = Math.trunc(1000 * (60 * signature) / bpm);
    this.setState(prevState => ({
      params: {
        ...prevState.params,
        bpm: bpmm,
        seconds
      }
    }))
  };

  // Function to handle the updates of the time signature parameter
  handleSignatureUpdate = (timeSignature: number) => {
    const { bpm, signature } = this.state.params;
    const seconds = Math.trunc(1000 * (60 * signature) / bpm);
    this.setState(prevState => ({
      params: {
        ...prevState.params,
        signature: timeSignature,
        seconds,
      }
    }))
  };

  // Function to add new midi Events to the list of events
  handleMidiEventsUpdate = (newEvent: IMidiEvent['event']) => {
    this.setState(prevState => ({
      midiEvents: [...prevState.midiEvents, newEvent]
    }));
    console.log('new event added: ', newEvent)
  };

  // Function select the number of octaves and the initial one
  handleKeyboardOctaves = (initial: number, number: number) => {
    this.setState({ 
      initialOctave: initial,
      numberOfOctaves: number
    });
  };

  // Function to change the instrument that is playing
  setInstrumentName = (instrumentName: InstrumentName) => {
    this.setState({ instrumentName });
  };

  // Function to change the first event of the buffer
  setFirstEvent = (firstEvent: number) => {
    this.setState({ firstEvent });
  };

  // Function to change the first event of the buffer
  setFirstEventTime = (firstEventTime: number) => {
    this.setState({ firstEventTime });
  };

  // Function to change the playing state, to start or stop sending the buffers
  handleIsPlaying = (isPlaying: boolean) => {
    this.setState({ isPlaying });
    if (isPlaying) {
      this.setState({ midiEvents: [] });
    };
  };

  setMode = (mode: string) => {
    this.setState({ mode, isPlaying: false });
  };

  setMetronomeState = (isPlaying: boolean) => {
    this.setState({playMetronome: isPlaying});
  }

  render () {
    return (
      <div className="appcontainer">
        <div className="menu-bar">
          <Metronome
            signature={this.state.params.signature} 
            setSignature={this.handleSignatureUpdate}
            bpm={this.state.params.bpm}
            setBPM={this.handleBpmUpdate}
            isPlaying={this.state.playMetronome}
            setMetronomeState={this.setMetronomeState}
            handleSubmit={this.handleSubmit}
          />
          <KeyboardForm 
            instrumentName={this.state.instrumentName}
            initial={this.state.initialOctave}
            number={this.state.numberOfOctaves}
            setInstrumentName={this.setInstrumentName}
            changeOctave={this.handleKeyboardOctaves}
          />
          <ModeForm 
            mode={this.state.mode}
            setMode={this.setMode}
          />
          <div className="menu-option about"> About </div>
        </div>
        <div className='apptitle'>On-the-fly Music Transcription</div>
        <div>
        {(this.state.params.seconds===0)?
          <div className='directions'>
            Please select a time signature and / or a tempo in the menu above.
            <br/> 
            You can also select the instrument audio and configure the screen keyboard.
          </div>:
          <MusicScore/>
        }
          {(this.state.params.seconds===0)?
            <div></div>:
            <div className="sound screen-keyboard">
              <SoundFontProvider 
                instrumentName={this.state.instrumentName}
                hostname={'https://d1pzp51pvbm36p.cloudfront.net'}
                soundfont="MusyngKite"
                format="mp3"
                onLoad={null}
                isMidi={true}
                addMidiEvent={this.handleMidiEventsUpdate}
                initialOctave={this.state.initialOctave}
                numberOfOctaves= {this.state.numberOfOctaves}
                handleIsPlaying={this.handleIsPlaying}
                firstEventTime={this.state.firstEventTime}
                setFirstEventTime={this.setFirstEventTime}
                mode={this.state.mode}
              />
              <Timer
                seconds={this.state.params.seconds}
                midiEvents={this.state.midiEvents}
                firstEvent={this.state.firstEvent}
                setFirstEvent={this.setFirstEvent}
                isPlaying={this.state.isPlaying}
              />
            </div>
          }
        </div>
      </div>
    );
  }
}

export default App;