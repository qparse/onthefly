import React from "react";

interface IProps {
    mode: string;
    setMode: (mode:string) => void;
}

const ModeForm: React.FC<IProps> = ({mode, setMode}) => {
    const handleSelectChange = (e: React.ChangeEvent<HTMLSelectElement>) : void => {
        setMode(e.target.value);
    }

    return (
        <div className="menu-option mode">
            <label>Keyboard type: </label>
            <select name="mode" className='mode' value={mode} onChange={handleSelectChange}>
                <option value="MIDI">MIDI Keyboard</option>
                <option value="screen">Screen Keyboard</option>
            </select>
        </div>
    )
}

export default ModeForm;