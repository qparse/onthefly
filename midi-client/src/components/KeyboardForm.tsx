import React, { useState } from "react";
import { InstrumentName } from 'soundfont-player';

const instrumentNames = require('./Audio/instruments.json');

interface IProps {
    instrumentName: any;
    initial: number;
    number: number;
    setInstrumentName: (name: InstrumentName) => void;
    changeOctave: (initial: number, number: number) => void;
  }

const KeyboardForm: React.FC<IProps> = ({instrumentName, initial, number, setInstrumentName, changeOctave}) => {
    const [name, setName] = useState(instrumentName);
    const [intialO, setIntialO] = useState(initial);
    const [numberO, setNumberO] = useState(number);

    // Function that handles the changes of the instrument sound
    const handleSelectChange = (e: React.ChangeEvent<HTMLSelectElement>) : void => {
        setName(e.target.value);
        let newInstrument: InstrumentName = e.target.value as InstrumentName;
        setInstrumentName(newInstrument);
    };

    // Function that handles the changes of the number of octaves and the initial octave.
    const handleOctaveChange = (e: React.ChangeEvent<HTMLSelectElement>) : void => {
        if (e.target.name === "number-octaves") {
            setNumberO(parseInt(e.target.value));
            changeOctave(initial, parseInt(e.target.value));
        } else if (e.target.name === "initial-octave") {
            setIntialO(parseInt(e.target.value));
            changeOctave(parseInt(e.target.value),number);
        }
    };

    return (
        <div className="menu-option keyboard">
            <div className="kb-instrument-selection">
                <label>Instrument: </label>
                <select name="instrument-name" className="instrument-name-input" value={instrumentName} onChange={handleSelectChange}>
                {Object.keys(instrumentNames).map((key:string) => {
                    return <option key={instrumentNames[key]} value={instrumentNames[key]}>{key}</option>
                })}
                </select>
            </div>
            <div className="kb-number-octaves-selection">
                <label>Nº of Octaves </label>
                <select name="number-octaves" className="number-octaves-input" value={number} onChange={handleOctaveChange}>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                </select>
            </div>
            <div className="kb-init-octaves-selection">
                <label>Initial Octave </label>
                <select name="initial-octave" className="initial-octave-input" value={initial} onChange={handleOctaveChange}>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                </select>
            </div>
        </div>
    )
}

export default KeyboardForm;