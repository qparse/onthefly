import React from "react";
import WebMidi, { InputEventMidimessage } from 'webmidi';
// import Timer from './TimerLogic';

interface IMidiEvent {
  event: {
    flag: string,
    key: number,
    velocity: number,
    timestamp: number
  }
}

interface IProps {
  addMidiEvent: (newEvent: IMidiEvent['event']) => void;
  playNote: (midiNumber: any) => void;
  stopNote: (midiNumber: any) => void;
  resumeAudio: () => void;
  stopAllNotes: () => void;
  handleIsPlaying: (isPlaying: boolean) => void;
  firstEventTime: number;
  setFirstEventTime: (firstEventTime: number) => void;
}

interface IState {
  midiConnected: boolean;
}

class MIDIComponent extends React.Component<IProps, IState> {
  state: IState = {
    midiConnected: true,
  };

  componentDidMount() {
    try {
      WebMidi.enable((err) => {
        if (err) {
          console.log("WebMidi could not be enabled.", err);
          this.setState({midiConnected: false});
        } else {
          // Play and Stop the recording
          try {
            WebMidi.inputs[0].addListener('midimessage', 'all',
             (e: InputEventMidimessage) => {
              // Start playing
              if (e.data[0] === 191 && e.data[1] === 107 && e.data[2] === 0) {
                this.props.handleIsPlaying(true);
                console.log('Started playing');
                this.props.setFirstEventTime(e.timestamp);
  
              // Stop playing
              } else if (e.data[0] === 191 && e.data[1] === 105 && e.data[2] === 0) {
                this.props.handleIsPlaying(false);
                console.log('Stopped playing')
  
              // ON event
              } else if (e.data[0] === 144 && e.data[2] !== 0) {
                const midiEvent: IMidiEvent['event'] = {
                  'flag': 'ON',
                  'key': e.data[1],
                  'velocity': e.data[2],
                  'timestamp': e.timestamp - this.props.firstEventTime
                };
                this.props.playNote(e.data[1]);
                this.props.addMidiEvent(midiEvent);
  
              // OFF event
              } else if (e.data[0] === 128 || (e.data[0] === 144 && e.data[2] === 0)) {
                const midiEvent: IMidiEvent['event'] = {
                  'flag': 'OFF',
                  'key': e.data[1],
                  'velocity': e.data[2],
                  'timestamp': e.timestamp - this.props.firstEventTime
                };
                this.props.stopNote(e.data[1]);
                this.props.addMidiEvent(midiEvent);
              }
            });
          } catch {
            this.setState({midiConnected: false});
          }
        }
      });
    } catch {
      this.setState({midiConnected: false});
    }
  };

  render () {
    return (
      <div>
        {this.state.midiConnected?
          '':
          'MIDI keyboard not connected'
        }
      </div>
  )
  }
}

export default MIDIComponent;