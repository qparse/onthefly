// See https://github.com/danigb/soundfont-player
// for more documentation on prop options.
import React from 'react';
import Soundfont, { InstrumentName, Player } from 'soundfont-player';
import Keyboard from '../Keyboard';
import MIDIComponent from '../MIDIComponent';
import { IMidiEvent } from '../../App';

interface IProps {
    instrumentName: InstrumentName;
    hostname: string;
    format: "mp3" | "ogg";
    soundfont: "MusyngKite" | "FluidR3_GM";
    onLoad: any;
    isMidi: boolean;
    addMidiEvent: (newEvent: IMidiEvent['event']) => void;
    initialOctave: number;
    numberOfOctaves: number;
    handleIsPlaying: (isPlaying: boolean) => void;
    firstEventTime: number;
    setFirstEventTime: (firstEventTime: number) => void;
    mode: string;
}

interface IState {
  activeAudioNodes: {
    [midiNumber: number]: Player
  };
  instrument?: Player;
}

class SoundFontProvider extends React.Component<IProps, IState> {
    audioContext: any = AudioContext && new AudioContext();
    constructor(props: any) {
        super(props);
        this.state = {
          activeAudioNodes: {},
          instrument: undefined,
        };
        this.loadInstrument = this.loadInstrument.bind(this);
        this.playNote = this.playNote.bind(this);
        this.stopNote = this.stopNote.bind(this);
        this.resumeAudio = this.resumeAudio.bind(this);
        this.stopAllNotes = this.stopAllNotes.bind(this);
      }
  

  componentDidMount() {
    // Load the correct instrument
    this.loadInstrument(this.props.instrumentName);
  }

  componentDidUpdate(prevProps: any, prevState: any) {
    // Check if the name of the instrument has changed
    if (prevProps.instrumentName !== this.props.instrumentName) {
      this.loadInstrument(this.props.instrumentName);
    }

    // Check if the instrument object has changed
    if (prevState.instrument !== this.state.instrument) {
      if (!this.props.onLoad) {
        return;
      }
      this.props.onLoad({
        playNote: this.playNote,
        stopNote: this.stopNote,
        stopAllNotes: this.stopAllNotes,
      });
    }
  }

  // Function to load the selected instrument
  loadInstrument = (instrumentName: InstrumentName) => {
    this.setState({
      instrument: undefined,
    });

    // create a new instrument instance
    Soundfont.instrument(this.audioContext, instrumentName)
          .then((instrument) => {
            this.setState({ instrument });
    });
  };

  // Function to play a note
  playNote = (midiNumber: any) => {
    this.resumeAudio().then(() => {
      // Save the played key as an audio node in the state
      const audioNode = this.state.instrument && this.state.instrument.play(midiNumber);
      this.setState({
        activeAudioNodes: Object.assign({}, this.state.activeAudioNodes, {
          [midiNumber]: audioNode,
        }),
      });
    });
  };

  // Function to stop a note
  stopNote = (midiNumber: number) => {
    this.resumeAudio().then(() => {
      // Check if the note is saved as an audio node
      if (this.state.activeAudioNodes[midiNumber]) {
        const audioNode = this.state.activeAudioNodes[midiNumber];
        // Stop the audio and delete the note from the state
        audioNode.stop();
        this.setState({
            activeAudioNodes: Object.assign({}, this.state.activeAudioNodes, { [midiNumber]: null }),
        });
      } else {
          return
      }
    });
  };

  // Function to resume the audio
  resumeAudio = () => {
    if (this.audioContext.state === 'suspended') {
      return this.audioContext.resume();
    } else {
      return Promise.resolve();
    }
  };

  // Clear any residual notes that don't get called with stopNote
  stopAllNotes = () => {
    this.audioContext.resume().then(() => {
      const activeAudioNodes = Object.values(this.state.activeAudioNodes);
      // iterate over the audio nodes to stop them
      activeAudioNodes.forEach((node) => {
        if (node) {
          node.stop();
        }
      });
      // Delete all the nodes from the state
      this.setState({
        activeAudioNodes: {},
      });
    });
  };

  render() {
    return (
      <div>
        {(this.props.mode === 'MIDI')?
          <MIDIComponent 
            addMidiEvent={this.props.addMidiEvent}
            playNote={this.playNote}
            stopNote={this.stopNote}
            resumeAudio={this.resumeAudio}
            stopAllNotes={this.stopAllNotes}
            handleIsPlaying={this.props.handleIsPlaying}
            firstEventTime={this.props.firstEventTime}
            setFirstEventTime={this.props.setFirstEventTime}
          />:
          <Keyboard 
            addMidiEvent={this.props.addMidiEvent}
            playNote={this.playNote}
            stopNote={this.stopNote}
            resumeAudio={this.resumeAudio}
            stopAllNotes={this.stopAllNotes}
            initialOctave={this.props.initialOctave}
            numberOfOctaves= {this.props.numberOfOctaves}
            handleIsPlaying={this.props.handleIsPlaying}
        />
        }
      </div>
    )
  }
}

export default SoundFontProvider;