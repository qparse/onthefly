import React, {useCallback, useEffect, useState} from "react";
import { IMidiEvent } from '../App'

interface IProps {
  seconds: number;
  midiEvents: IMidiEvent["event"][];
  firstEvent: number;
  setFirstEvent: (firstEvent: number) => void;
  isPlaying: boolean;
}
const Timer: React.FC<IProps> = ({ seconds, midiEvents, firstEvent, setFirstEvent, isPlaying }) => {
  // Callback to send the MIDI messages buffer to the server.
  const sendData = useCallback(
    () => {
      console.log('Enviando ...', firstEvent)
      // await axios.post('url para enviar mensaje', midiEvents)
      console.log(midiEvents.slice(firstEvent))
    },
    [midiEvents, firstEvent]
  );

  // Effect to set a new time interval in which the messages will be sent
  const [timeInterval, setTimeInterval] = useState(0)
  useEffect(() => {
    if (isPlaying) {
      const timer = setInterval(() => {
        setTimeInterval(timeInterval => timeInterval+1)
      }, seconds);
      return () => clearInterval(timer);
    }
  }, [isPlaying, seconds, timeInterval]);

  // Callback to send the data to the server and restart the buffer.
  const timer = useCallback(
    ()=>{
      if (isPlaying) {
        sendData();
        setFirstEvent(midiEvents.length);
        console.log('working')
      }
    },
    [isPlaying, midiEvents, setFirstEvent, sendData]
  )

  // Function that runs every X amount of seconds.
  useEffect(() => {
    timer()
  }, [timeInterval])

  return (
    <div>
    </div>
  )
}

export default Timer;
