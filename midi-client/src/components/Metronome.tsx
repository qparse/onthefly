import React, { Component } from "react";

const click1 = "//daveceddia.com/freebies/react-metronome/click1.wav";
const click2 = "//daveceddia.com/freebies/react-metronome/click2.wav";

interface IState {
  isPlaying: boolean;
  count: number;
  bpm: number;
  beatsPerMeasure: number;
};

interface IProps {
  bpm: number;
  isPlaying: boolean;
  setBPM: (bpm: number) => void;
  setMetronomeState: (isPlaying: boolean) => void;
  signature: number;
  setSignature: (signature: number) => void;
  handleSubmit: (signature: number, bpm: number) => void;
};


class Metronome extends Component<IProps, IState> {
  click1: any = new Audio(click1);
  click2: any = new Audio(click2);
  timer: any = undefined;
  constructor(props: any) {
    super(props);

    this.state = {
      isPlaying: this.props.isPlaying,
      count: 0,
      bpm: this.props.bpm,
      beatsPerMeasure: 4
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleDecreaseTempo = this.handleDecreaseTempo.bind(this);
    this.handleIncreaseTempo = this.handleIncreaseTempo.bind(this);
    this.handleSelectChange = this.handleSelectChange.bind(this);
    this.playClick = this.playClick.bind(this);
    this.startStop = this.startStop.bind(this);
  }

  // Function that handles the changes of the tempo
  handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) : void => {
    const bpm = parseInt(e.target.value);

    if (this.props.isPlaying) {
      // stop old timer and start a new one
      clearInterval(this.timer);
      this.timer = setInterval(this.playClick, (60 / bpm) * 1000);
      // set the new bpm
      // and reset the beat counter
      this.setState({
        count: 0,
        bpm
      });
      // set the new bpm in the parent
      this.props.setBPM(bpm);
    } else {
      // otherwise, just update the bpm
      this.setState({ bpm });
    }
  }

  // Function that handles the decreasing of one bpm in the tempo with the - button
  handleDecreaseTempo = () : void => {
    let aux = this.state.bpm - 1;

    if (this.props.isPlaying) {
      // stop old timer and start a new one
      clearInterval(this.timer);
      this.timer = setInterval(this.playClick, (60 / aux) * 1000);
      // set the new bpm
      // and reset the beat counter
      this.setState({
        count: 0,
        bpm: aux
      });
      // set the new bpm in the parent
      this.props.setBPM(aux);
    } else {
      // otherwise, just update the bpm
      this.setState({ bpm: aux });
    }
  };
  
  // Function that handles the increasing of one bpm in the tempo with the + button.
  handleIncreaseTempo = () : void => {
    let aux = this.state.bpm + 1;

    if (this.props.isPlaying) {
      // stop old timer and start a new one
      clearInterval(this.timer);
      this.timer = setInterval(this.playClick, (60 / aux) * 1000);
      // set the new bpm
      // and reset the beat counter
      this.setState({
        count: 0,
        bpm: aux
      });
      // set the new bpm in the parent
      this.props.setBPM(aux);
    } else {
      // otherwise, just update the bpm
      this.setState({ bpm: aux });
    }
  };

  // Function that plays the sound of one click of the metronome
  playClick = () => {
    const { count, beatsPerMeasure } = this.state;

    // alternate click sounds
    if (count % beatsPerMeasure === 0) {
      this.click2.play();
    } else {
      this.click1.play();
    }

    // keep track of which beat we're on
    this.setState(state => ({
      count: (state.count + 1) % state.beatsPerMeasure
    }));
  };

  // Function that handles the clicks on the metronome button.
  // It sends the selected time signature and bpm values to the parent, 
  // and starts or stops the sound of the metronome.
  handleClick = () => {
    this.props.handleSubmit(this.props.signature, this.props.bpm)
    this.startStop();
  };

  // Function that handle the changes of the time signature values
  handleSelectChange = (e: React.ChangeEvent<HTMLSelectElement>) : void => {
    this.props.setSignature(parseInt(e.target.value));
}

  // Function that start or stop the sound of the metronome
  startStop = () => {
    if (this.props.isPlaying) {
      // stop the timer
      clearInterval(this.timer);
      this.props.setMetronomeState(false);
      this.setState({
        isPlaying: false
      });
    } else {
      // start a timer with current bpm
      this.timer = setInterval(this.playClick, (60 / this.state.bpm) * 1000);
      this.props.setMetronomeState(true);
      this.setState(
        {
          count: 0,
          isPlaying: true
        },
        // play a click immediately (after setState finishes)
        this.playClick
      );
    }
  };

  render() {

    return (
      <div className="menu-option time-tempo">
        <div className="time-signature-selection">
          <div>Time signature: </div>
          <select name="signature" className='params-input' value={this.props.signature} onChange={this.handleSelectChange}>
            <option value="2">2/4</option>
            <option value="3">3/4</option>
            <option value="4">4/4</option>
          </select>
        </div>

        <div className="tempo-selection">
          <div>Tempo (BPM):</div>
          <div className="adjust-tempo-btn decrease-tempo" onClick={this.handleDecreaseTempo}>
            -
          </div>
            <input 
                name="bpm"
                type="number" 
                min="30" 
                max="240" 
                step="1" 
                value={this.state.bpm}
                onChange={this.handleInputChange}
            />
          <div className="adjust-tempo-btn increase-tempo" onClick={this.handleIncreaseTempo}>
            +
          </div>
        </div>
        <div className="send-button-selection" onClick={this.handleClick}>
          {this.state.isPlaying?
            "Stop":
            "Start"} 
          <br/> 
          Metronome
        </div>
      </div>
    );
  }
}

export default Metronome;