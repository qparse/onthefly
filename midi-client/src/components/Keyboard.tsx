import React, { useEffect, useState } from "react";
import {IMidiEvent} from "../App"

interface IKeys {
    [key: string]: number[];
  }

interface IProps {
    addMidiEvent: (newEvent: IMidiEvent['event']) => void;
    playNote: (midiNumber: any) => void;
    stopNote: (midiNumber: any) => void;
    resumeAudio: () => void;
    stopAllNotes: () => void;
    initialOctave: number;
    numberOfOctaves: number;
    handleIsPlaying: (isPlaying: boolean) => void;
}

const Keyboard: React.FC<IProps> = ({addMidiEvent, playNote, stopNote, initialOctave, numberOfOctaves, handleIsPlaying}) => {
    const [keysNumbers, setKeysNumbers] = useState<IKeys>();
    const [initialTime, setInitialTime] = useState<number>();
    let yPos = 0;

    // Function that returns a dictionary with the position of each key of the keyboard
    const createKeyNumbers = (initialOctave:number, numberOfOctaves: number): IKeys => {
        let whiteKeysArray = [];
        let blackKeysArray = [];
        for (let octave = initialOctave; octave < initialOctave+numberOfOctaves; octave++) {
            for (let i = (octave*12); i < ((octave*12) + 5); i += 2) {
                whiteKeysArray.push(i)
            }
            for (let j = ((octave*12) + 5); j < ((octave*12) + 12); j += 2) {
                whiteKeysArray.push(j)
            }
            for (let k = ((octave*11) + (octave + 1)); k < ((octave*11) + (octave + 5)); k += 2) {
                blackKeysArray.push(k)
            }
            for (let k = ((octave*11) + (octave + 6)); k < ((octave*11) + (octave + 11)); k += 2) {
                blackKeysArray.push(k)
            }
        }
        return {
            'white': whiteKeysArray, 
            'black': blackKeysArray
        };
    };

    // Function that creates a MIDI message associated with the key pressed, 
    // adds the message to the buffer, and emits the associated sound
    const handleMouseDown = (event: any) => {
        const timestamp = Date.now();
        let midiEvent: any;
        if (initialTime) {
            midiEvent = {
                'flag': 'ON',
                'key': event.target.id,
                'velocity': 127,
                'timestamp': timestamp-initialTime
              };
        } else {
            setInitialTime(timestamp);
            midiEvent = {
                'flag': 'ON',
                'key': event.target.id,
                'velocity': 127,
                'timestamp': 0
              };
        }
        addMidiEvent(midiEvent);
        playNote(event.target.id);
    }

    // Function that creates a MIDI message associated with the release of the key,
    // adds the message to the buffer, and stops the associated sound
    const handleMouseUp = (event: any) => {
        const timestamp = Date.now();
        let midiEvent: any;
        if (initialTime) {
            midiEvent = {
                'flag': 'OFF',
                'key': event.target.id,
                'velocity': 0,
                'timestamp': timestamp-initialTime
              };
        } else {
            setInitialTime(timestamp);
            midiEvent = {
                'flag': 'OFF',
                'key': event.target.id,
                'velocity': 0,
                'timestamp': 0
              };
        }
        addMidiEvent(midiEvent);
        stopNote(event.target.id);
    }

    // Function that sets the playing state of the parent to true, 
    // in order to let it know that the key is pressed 
    const handlePlay = () => {
        handleIsPlaying(true);
    }

    // Function that sets the playing state of the parent to false, 
    // in order to let it know that the key is released 
    const handleStop = () => {
        handleIsPlaying(false);
    }

    // Callback that is called everytime the number of octaves or the initial octave change,
    // creates the new dictionary of keys, and set it as the new state.
    useEffect(()=>{
        const kn = createKeyNumbers(initialOctave, numberOfOctaves);
        setKeysNumbers(kn);
    }, [numberOfOctaves, initialOctave]);


return (
    <div>
        <div className="screen-recording">
            <div className="button-play" onClick={handlePlay}>
                <svg viewBox="0 0 100 100" version="1.1" xmlns="http://www.w3.org/2000/svg" height="50" width="50">
                    <circle cx="50" cy="50" r="40" fill="#E64F3E"/>
                </svg>
            </div>
            <div className="button-stop" onClick={handleStop}>
                <svg viewBox="0 0 100 100" version="1.1" xmlns="http://www.w3.org/2000/svg" height="50" width="50">
                    <rect width="80" height="80" fill="#4B4B4B"/>
                </svg>
            </div>
            <div></div>
        </div>
        <div id="MIDI-keyboard">
        <svg width="100%" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" viewBox={`0 0 ${560*numberOfOctaves} 400`}>
            {keysNumbers &&
            keysNumbers.white.map((keyNumber, i) => {
                return (
                    <rect key={`${keyNumber}`} id={`${keyNumber}`} className="piano-key white-key" 
                          x={`${i*80}`} y="0" width="80" height="400" 
                          onMouseDown={handleMouseDown}
                          onMouseUp={handleMouseUp}
                        //   onMouseOut={handleMouseUp}
                          ></rect>
            )})}
            {keysNumbers &&
            (keysNumbers.black).map((keyNumber, i) => {
                if (i === 0) yPos = 60
                else if (( i%5 === 0 ) || ( i%5 === 2 )) yPos += 160
                else yPos += 80; 
                return (
                    <rect key={`${keyNumber}`} id={`${keyNumber}`} className="piano-key black-key" 
                          x={`${yPos}`} y="0" width="40" height="250"
                          onMouseDown={handleMouseDown}
                          onMouseUp={handleMouseUp}
                        //   onMouseOut={handleMouseUp}
                          ></rect>
            )})}
        </svg>
    </div>
    </div>
)
}

export default Keyboard;