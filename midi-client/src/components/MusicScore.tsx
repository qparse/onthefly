import React from 'react';

class MusicScore extends React.Component<any, any> {
    componentDidMount() {
        const file = "'https://www.verovio.org/editor/brahms.mei'"
        const s = document.createElement('script');
        // s.src = "./verovio_test.js";
        s.async = true;
        s.innerHTML = `\
        function verovioCallback(file){\n\
            let tk = new verovio.toolkit();\n\
            console.log("Verovio has loaded!");\n\
            tk.setOptions({\n\
                scale: 20,\n\
                adjustPageWidth: true,\n\
                breaks: "none",\n\
                footer: "none",\n\
                header: "none",\n\
            });\n\
            tk.renderToSVG\n\
            fetch(file)\n\
              .then( (response) => response.text() )\n\
              .then( (meiXML) => {\n\
                let svg = tk.renderData(meiXML, {});\n\
                document.getElementById("notation").innerHTML = svg;\n\
              });\n\
        }\n\
        if( document.readyState !== 'loading' ) {\n\
            verovioCallback(${file})\n\
        } else {\n\
            document.addEventListener('DOMContentLoaded', function () {\n\
                verovioCallback(${file})\n\
            });\n\
          };`
        document.body.appendChild(s);
    }

    render () {
        return <div id="notation"></div>
    }
}
export default MusicScore;